from setuptools import setup, find_packages
setup(
    name = 'babeldocker',
    version = '2.4.1',
    packages = find_packages(),
    scripts = [
        'scripts/obabel',
    ],
)
