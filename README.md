# babel_docker

Delivers [Open Babel](https://open-babel.readthedocs.io/en/latest/index.html) via a Docker container - just a thin 
wrapper around the docker provided by [InformaticsMatters](https://hub.docker.com/r/informaticsmatters/obabel/)

## Installation

Easiest via pip:
```
% pip install git+https://bitbucket.org/claughton/babel_docker.git
```
This installs a single command `obabel` in your path.


## Usage

See the Open Babel documentation [here](https://open-babel.readthedocs.io/en/latest/index.html)

